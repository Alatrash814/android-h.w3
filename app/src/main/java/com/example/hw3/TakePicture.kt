package com.example.hw3

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.ImageView
import kotlinx.android.synthetic.main.takepicture.*

class TakePicture : AppCompatActivity(), View.OnClickListener {

    internal var save = this
    internal var cameraRequestCode = 1

    public override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.takepicture)


        Button.setOnClickListener(this)
        backPress.setOnClickListener(this)

    }//onCreate


    override fun onClick(v: View) {

        if (v!!.id == backPress.id)
            save.finish()


        if (v!!.id == Button.id) {

            val temp = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (temp.resolveActivity(packageManager) != null)
                startActivityForResult(temp, cameraRequestCode)

                                 }//if statement

                                  }//onClick

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == cameraRequestCode && requestCode == 1) {
            val bundle = data!!.extras
            val imageBitmap = bundle!!.get("data") as Bitmap
            imageView.setImageBitmap(imageBitmap)
                                                                  }//if statement

                                                                                    }//onActivityResult

                                                                   }//TakePicture(class)
