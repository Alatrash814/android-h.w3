package com.example.hw3

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.email.*
import java.util.ArrayList

class email : AppCompatActivity() , View.OnClickListener {


    var message: String = ""
    var sendTo : List <String> = ArrayList ()
    var allCCEmails: List<String> = ArrayList()
    var allBCCEmails: List<String> = ArrayList()
    var CCFlag : Boolean = false
    var BCCFlag : Boolean = false
    var save = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.email)

        send.setOnClickListener(this)
        back.setOnClickListener(this)
                                                       }//onCreate

    override fun onClick(v: View?) {

        if (v!!.id == send.id){

            if (!messageText.text.toString().isEmpty() && !Label.text.toString().isEmpty()){

                message = messageText.text.toString()

                sendTo = Label.text.toString().split("\n")

                if (!CCLabel.text.toString().isEmpty()) {
                    allCCEmails = CCLabel.text.toString().split("\n")
                    CCFlag = true
                                                        }//if statement

                if (!BCCLabel.text.toString().isEmpty()) {
                    allBCCEmails = BCCLabel.text.toString().split("\n")
                    BCCFlag = true
                                                         }//if statement

                val temp = Intent(Intent.ACTION_SENDTO)

                temp.type = "text/html"

                temp.putExtra(Intent.EXTRA_TEXT,message)
                temp.putExtra(Intent.EXTRA_SUBJECT, messageSubject.text.toString())

                var UriString : String = "mailto:"

                for (i in 0 until sendTo.size)
                    UriString += sendTo[i]

                temp.data = Uri.parse(UriString)

                if (CCFlag)
                        temp.putExtra(Intent.EXTRA_CC , allCCEmails.toTypedArray())

                if (BCCFlag)
                        temp.putExtra(Intent.EXTRA_BCC , allBCCEmails.toTypedArray())


                startActivity(temp)
                                                        }//if statement

            else
                Toast.makeText(save,"You should enter message and emails !",Toast.LENGTH_SHORT).show()

                              }//if statement

        if (v!!.id == back.id)
            save.finish()
                                   }//onClick

                                                          }//email (class)