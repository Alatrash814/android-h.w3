package com.example.hw3

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.telephony.SmsManager
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.sms.*
import java.util.ArrayList

class sms : AppCompatActivity() , View.OnClickListener {

    var message : String = ""
    var SMSRequestCode : Int = 100
    var allNumbers : List <String> = ArrayList()
    var save = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.sms)

        send.setOnClickListener(this)
        back.setOnClickListener(this)
                                                       }//onCreate

    override fun onClick(v: View?) {

        if (v!!.id == send.id){

            if(!NumbersLabel.text.toString().isEmpty() && !messageText.text.toString().isEmpty()){

                message = messageText.text.toString()
                allNumbers = NumbersLabel.text.toString().split("\n")
                if(ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS)!= PackageManager.PERMISSION_GRANTED)
                    ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.SEND_SMS),SMSRequestCode)

                else {

                    val man = SmsManager.getDefault()
                    for (number in allNumbers)
                        man.sendTextMessage(number, null, message, null, null)

                     }//else statement
                                                                                                 }//if statement

            else
                Toast.makeText(save,"You should enter message and phone number(s) !",Toast.LENGTH_SHORT).show()

                              }//if statement (send click action)

        if (v!!.id == back.id)
            save.finish()

                                   }//onClick

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {

        if (requestCode == SMSRequestCode && grantResults.size == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
            grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            val man = SmsManager.getDefault()
            for (number in allNumbers)
                man.sendTextMessage(number, null, message, null, null)
                                                                  }//if statement

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
                                                                                                                  }//onRequestPermissionsResult

                                                       }//sms (class)