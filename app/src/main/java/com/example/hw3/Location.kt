package com.example.hw3

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.position.*
import android.content.Intent
import android.net.Uri
import android.widget.Toast

class Location : AppCompatActivity() , View.OnClickListener {

    var save = this

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.position)

        SearchByNameClick.setOnClickListener(this)
        SearchByLocationClick.setOnClickListener(this)
        back.setOnClickListener(this)
                                                        }//onCreate



    override fun onClick(v: View?) {

        if (v!!.id == SearchByLocationClick.id){

            if (!XLocation.text.toString().isEmpty() && !YLocation.text.toString().isEmpty()){

                val gmmIntentUri = Uri.parse("geo:"+XLocation.text.toString() + "," + YLocation.text.toString())
                val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                mapIntent.setPackage("com.google.android.apps.maps")
                startActivity(mapIntent)
                                                                                             }//if statement

            else
                Toast.makeText(save,"You should enter X-Coordinate and Y-Coordinate !",Toast.LENGTH_SHORT).show()
                                          }//if statement

        if (v!!.id == SearchByNameClick.id){

            if (!searchByNameLabel.text.toString().isEmpty()) {
                val gmmIntentUri = Uri.parse("geo:0,0?q="+searchByNameLabel.text.toString())
                val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                mapIntent.setPackage("com.google.android.apps.maps")
                startActivity(mapIntent)
                                                              }//if statement

            else
                Toast.makeText(save,"You should enter location name !",Toast.LENGTH_SHORT).show()
                                           }//if statement

        if (v!!.id == back.id)
            this.finish()

                                   }//onClick

                                                            }//Location(class)