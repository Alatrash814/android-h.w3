package com.example.hw3

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() , View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        SMS.setOnClickListener(this)
        Email.setOnClickListener(this)
        Position.setOnClickListener(this)
        Camera.setOnClickListener(this)
                                                       }//onCreate

    override fun onClick(v: View?) {

        when {

            v!!.id == SMS.id -> {

                startActivity(Intent (this,sms::class.java))

                                }//case 1 statement (SMS Click Action)

            v!!.id == Email.id -> {

                startActivity(Intent (this,email::class.java))

                                  }//case 2 statement (Email Click Action)

            v!!.id == Position.id -> {

                startActivity(Intent (this,Location::class.java))

                                     }//case 3 statement (Position Click Action)

            v!!.id == Camera.id -> {

                startActivity(Intent(this,TakePicture::class.java))

                                   }//case 4 statement (Camera Click Action)

             }//when statement
                                   }//onClick

                                                                 }//MainActivity
